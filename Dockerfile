# run usage
# docker run -it --rm -v $PWD/api_data.json.real:/api_data.json quay.io/cmosetick/gitlab-api-tool

FROM python:3.7
WORKDIR /

RUN \
pip3 install python-gitlab

COPY gitlab_api_tool.py .
COPY api_data.json .
COPY api_projects.txt .

CMD ["/usr/local/bin/python3","gitlab_api_tool.py"]
